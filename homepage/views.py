from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'story3_home.html')

def about(request):
    return render(request, 'story3_about.html')

def resume(request):
    return render(request, 'story3_resume.html')

def contact(request):
    return render(request, 'story3_contact.html')